import firebase from "firebase";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCziba_5MUBu0bzbaWuIp9Lh_HgRl5e-Co",
  authDomain: "vue-chat-cb1f2.firebaseapp.com",
  databaseURL: "https://vue-chat-cb1f2.firebaseio.com",
  projectId: "vue-chat-cb1f2",
  storageBucket: "vue-chat-cb1f2.appspot.com",
  messagingSenderId: "815609647109",
  appId: "1:815609647109:web:1831551da2996823"
};

// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore();
